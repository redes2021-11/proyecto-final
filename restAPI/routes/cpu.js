const express = require('express');
const CPUInfoCtrl = require('../controllers/cpu');

const cpu = express.Router();

cpu.route('/average')
    .get(CPUInfoCtrl.getAverage);

cpu.route('/average-usage')
    .get(CPUInfoCtrl.getAverageUsage);

cpu.route('/free-percentage')
    .get(CPUInfoCtrl.getFreePercentage);

cpu.route('/count')
    .get(CPUInfoCtrl.getCount);

cpu.route('/model')
    .get(CPUInfoCtrl.getModel);

cpu.route('/load-average')
    .get(CPUInfoCtrl.getLoadAverage);

cpu.route('/load-average-time')
    .get(CPUInfoCtrl.getLoadAverageTime);

module.exports = cpu;
