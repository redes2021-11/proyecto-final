const express = require('express');
const OSInfoCtrl = require('../controllers/os');

const os = express.Router();

os.route('/original-os')
    .get(OSInfoCtrl.getOriginalOS);

os.route('/hostname')
    .get(OSInfoCtrl.getHostName);

os.route('/architecture')
    .get(OSInfoCtrl.getArchitecture);

os.route('/up-time')
    .get(OSInfoCtrl.getUptime);

os.route('/platform')
    .get(OSInfoCtrl.getPlatform);

os.route('/type')
    .get(OSInfoCtrl.getType);

module.exports = os;
