const osu = require('node-os-utils')
const net = osu.netstat

const netInfo = {
    async getStats(req, res) {
        let info = await net.stats();
        res.status(200).jsonp(info);
    },

    async getInOut(req, res) {
        let info = await net.inOut();
        res.status(200).jsonp(info);
    }
}

module.exports = netInfo;
