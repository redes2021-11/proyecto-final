const osu = require('node-os-utils')
const os = osu.os

const osInfo = {
    async getOriginalOS(req, res) {
        let info = await os.oos();
        res.status(200).jsonp(info);
    },

    async getHostName(req, res) {
        let info =  os.hostname();
        res.status(200).jsonp(info);
    },

    async getArchitecture(req, res) {
        let info = os.arch();
        res.status(200).jsonp(info);
    },

    async getUptime(req, res) {
        let info =  os.uptime();
        res.status(200).jsonp(info);
    },

    async getPlatform(req, res) {
        let info =  os.platform();
        res.status(200).jsonp(info);
    },

    async getType(req, res) {
        let info =  os.type();
        res.status(200).jsonp(info);
    },
}

module.exports = osInfo;
