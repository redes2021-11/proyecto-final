const TelegramBot = require('node-telegram-bot-api');
const token = '1659617051:AAHY71m7BOkxBT2kdQkCufDpeBS0zgXqf04';
const bot = new TelegramBot(token, { polling: true });
const axios = require('axios');

console.log('Starting Listen ChatBot');

const API = 'https://api.premed.com.mx/api';

bot.onText(/^\/start/, (msg) => {
    console.log('msg');
    const chatId = msg.chat.id;
    const username = msg.from.username;
    bot.sendMessage(chatId, `Hola ${username}, mi nombre es OutOfTimeBot, y estoy aquí para ayudarte`);
});

bot.onText(/^\/cpu-average/, async (msg) => {
    const chatId = msg.chat.id;
    const { data } = await axios.get(`${API}/cpu/average`);
    const message = `Total Idle: ${data.totalIdle}\n`
                  + `Total Tick: ${data.totalTick}\n`
                  + `avg Idle: ${data.avgIdle}\n`
                  + `avg Total: ${data.avgTotal}`
    bot.sendMessage(chatId, message);
});

bot.onText(/^\/cpu-usage/, async (msg) => {
    const chatId = msg.chat.id;
    const { data } = await axios.get(`${API}/cpu/average-usage`);
    const message = `Average Usage: ${data}`
    bot.sendMessage(chatId, message);
});

bot.onText(/^\/cpu-free/, async (msg) => {
    const chatId = msg.chat.id;
    const { data } = await axios.get(`${API}/cpu/free-percentage`);
    const message = `Porcentaje Libre: ${data} %`
    bot.sendMessage(chatId, message);
});

bot.onText(/^\/cpu-info/, async (msg) => {
    let chatId = msg.chat.id;
    const username = msg.from.username;
    bot.sendMessage(chatId, `Hola ${username}, estamos consultando la información del CPU`);
    const usage = await axios.get(`${API}/cpu/average-usage`);
    const free = await axios.get(`${API}/cpu/free-percentage`);
    const cores = await axios.get(`${API}/cpu/count`);
    const model = await axios.get(`${API}/cpu/model`);
    const message = `Usado: ${usage.data} %\n`
                  + `Libre: ${free.data} %\n`
                  + `Total de Cores: ${cores.data} cores\n`
                  + `Modelo: ${model.data}`;
    bot.sendMessage(chatId, message);
});

bot.onText(/^\/mem-info/, async (msg) => {
    let chatId = msg.chat.id;
    const username = msg.from.username;
    bot.sendMessage(chatId, `Hola ${username}, estamos consultando la información de Memoria RAM`);
    const { data } = await axios.get(`${API}/mem/info`);
    const message = `Memoria Total: ${data.totalMemMb} MB\n`
                  + `Memoria Usada: ${data.usedMemMb} MB\n`
                  + `Memoria Libre: ${data.freeMemMb} MB\n`
                  + `Porcentaje Libre: ${data.freeMemPercentage} %`;
    bot.sendMessage(chatId, message);
});

bot.onText(/^\/net-info/, async (msg) => {
    let chatId = msg.chat.id;
    const username = msg.from.username;
    bot.sendMessage(chatId, `Hola ${username}, estamos consultando la información de Red`);
    const { data } = await axios.get(`${API}/net/stats`);
    let message = "";
    data.forEach(interface => {
        const net_data = `Interface: ${interface.interface}\n`
                      + `Entrada: ${interface.inputBytes} Bytes\n`
                      + `Salida: ${interface.outputBytes} Bytes\n`;
        message += net_data;
    });
    bot.sendMessage(chatId, message);
});

bot.onText(/^\/os-info/, async (msg) => {
    let chatId = msg.chat.id;
    const username = msg.from.username;
    bot.sendMessage(chatId, `Hola ${username}, estamos consultando la información del SO`);
    const so = await axios.get(`${API}/os/original-os`);
    const hostname = await axios.get(`${API}/os/hostname`);
    const arch = await axios.get(`${API}/os/architecture`);
    const uptime = await axios.get(`${API}/os/up-time`);
    const platform = await axios.get(`${API}/os/type`);
    const message = `SO: ${so.data}\n`
                  + `Hostname: ${hostname.data}\n`
                  + `Arquitectura: ${arch.data}\n`
                  + `Uptime: ${uptime.data} seg\n`
                  + `Platform: ${platform.data}`;
    bot.sendMessage(chatId, message);
});
