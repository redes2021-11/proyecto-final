const osu = require('node-os-utils')
const cpu = osu.cpu;

const cpuInfo = {
    async getAverage(req, res) {
        let info = cpu.average();
        res.status(200).jsonp(info);
    },

    async getAverageUsage(req, res) {
        let info = await cpu.usage();
        res.status(200).jsonp(info);
    },

    async getFreePercentage(req, res) {
        let free = await cpu.free();
        res.status(200).jsonp(free);
    },

    async getCount(req, res) {
        let count = cpu.count();
        res.status(200).jsonp(count);
    },

    async getModel(req, res) {
        let model = cpu.model();
        res.status(200).jsonp(model);
    },

    async getLoadAverage(req, res) {
        let info = cpu.loadavg();
        res.status(200).jsonp(info);
    },

    async getLoadAverageTime(req, res) {
        let loadAverageTime = cpu.loadavgTime();
        res.status(200).jsonp(loadAverageTime);
    },
}

module.exports = cpuInfo;
