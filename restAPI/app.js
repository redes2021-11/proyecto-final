const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const CPURoutes = require('./routes/cpu');
const NetRoutes = require('./routes/net');
const MemRoutes = require('./routes/mem');
const OsRoutes = require('./routes/os');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

const router = express.Router();

router.get('/', (req, res) => {
    res.send("Hello World!");
});

app.use(router);

// API routes
app.use('/api/cpu', CPURoutes);
app.use('/api/net', NetRoutes);
app.use('/api/mem', MemRoutes);
app.use('/api/os', OsRoutes);

app.listen(3000, function() {
    console.log("Node server running on http://localhost:3000");
});
