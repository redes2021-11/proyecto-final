const osu = require('node-os-utils')
const mem = osu.mem;

const memInfo = {
    async getInfo(req, res) {
        let info = await mem.info();
        res.status(200).jsonp(info);
    },

    async getFree(req, res) {
        let free = await mem.free();
        res.status(200).jsonp(free);
    },

    async getUsed(req, res) {
        let used = await mem.used();
        res.status(200).jsonp(used);
    },

    async getTotal(req, res) {
        let total = mem.totalMem();
        res.status(200).jsonp(total);
    },
}

module.exports = memInfo;
