const express = require('express');
const MemInfoCtrl = require('../controllers/mem');

const mem = express.Router();

mem.route('/info')
    .get(MemInfoCtrl.getInfo);

mem.route('/free')
    .get(MemInfoCtrl.getFree);

mem.route('/used')
    .get(MemInfoCtrl.getUsed);

mem.route('/total')
    .get(MemInfoCtrl.getTotal);

module.exports = mem;
