const express = require('express');
const NetInfoCtrl = require('../controllers/net');

const net = express.Router();

net.route('/stats')
    .get(NetInfoCtrl.getStats);

net.route('/in-out')
    .get(NetInfoCtrl.getInOut);

module.exports = net;
